import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    drawer: null,
    headersMessage: [
      {
        sortable: false,
        text: 'edit',
        value: 'actionEdit',
      },
      {
        sortable: true,
        text: 'ID',
        value: '_id',
      },
      {
        sortable: true,
        text: 'First Name',
        value: 'firstName',
      },
      {
        sortable: true,
        text: 'Last Name',
        value: 'lastName',
      },
      {
        sortable: true,
        text: 'E-mail',
        value: 'mail',
      },
      {
        sortable: true,
        text: 'object',
        value: 'object',
        // align: 'left',
      },
      {
        sortable: true,
        text: 'message',
        value: 'message',
        // align: 'left',
      },
      {
        sortable: true,
        text: 'date',
        value: 'date',
        // align: 'left',
      },
    ],
    headersUsers: [
      {
        sortable: false,
        text: 'edit',
        value: 'actionEdit',
      },
      {
        sortable: true,
        text: 'ID',
        value: '_id',
      },
      {
        sortable: true,
        text: 'First Name',
        value: 'firstName',
      },
      {
        sortable: true,
        text: 'Last Name',
        value: 'lastName',
      },
      {
        sortable: true,
        text: 'E-mail',
        value: 'mail',
      },
      {
        sortable: true,
        text: 'homeAddress',
        value: 'homeAddress',
        // align: 'left',
      },
      {
        sortable: true,
        text: 'favorite Transport',
        value: 'favoriteTransport',
        // align: 'left',
      },
      /* {
        sortable: true,
        text: 'Interactions',
        value: 'interactions',
        align: 'right',
      }, */
      {
        sortable: true,
        text: 'Birth date',
        value: 'birthDate',
        align: 'right',
      },
    ],
    headersWaypoints: [
      {
        sortable: false,
        text: 'edit',
        value: 'actionEdit',
      },
      {
        sortable: true,
        text: 'ID',
        value: 'properties._id',
      },
      {
        sortable: true,
        text: 'Droped by',
        value: 'properties.username',
      },
      {
        sortable: true,
        text: 'Location : x',
        value: 'geometry.coordinates.0',
        align: 'left',
      },
      {
        sortable: true,
        text: 'Location : y',
        value: 'geometry.coordinates.1',
        align: 'left',
      },
      {
        sortable: true,
        text: 'Type',
        value: 'properties.type',
        align: 'left',
      },
      {
        sortable: true,
        text: 'Status',
        value: 'properties.status',
        align: 'right',
      },
      {
        sortable: true,
        text: 'infirmation',
        value: 'properties.infirmation',
        align: 'right',
      },
      {
        sortable: true,
        text: 'verification',
        value: 'properties.verification',
        align: 'right',
      },
      {
        sortable: true,
        text: 'Date (yyyy/mm/dd)',
        value: 'properties.date',
        align: 'left',
      },
    ],
  },
  mutations: {
    SET_DRAWER (state, payload) {
      state.drawer = payload
    },
  },
  actions: {

  },
})
